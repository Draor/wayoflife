﻿using UnityEngine;
using System.Collections;

// Game of life (http://www.tiikoni.net/gameoflife/).
public class Grid : MonoBehaviour {


	public GameObject cellPrefab;	// Simulaationappula.
	public GameObject[,] cells; 	// Soluruudukko
	GameObject[,] tmpCells;			// Tilapäinen soluruudukko, johon jokaisen vaiheen muutokset tehdään ja jolla korvataan alkuperäinen ruudukko kun kaikki muutokset on laskettu ja tehty.

	public int sizeofCellGrid = 10; // Soluruudukon koko; oletuskoko 10*10.
    

    

	// Use this for initialization
	void Start () {

		// Luodaan soluruudukot
		cells = new GameObject[sizeofCellGrid,sizeofCellGrid];
		tmpCells = new GameObject[sizeofCellGrid,sizeofCellGrid];


		FillCells ();


		setUpCells ();
	}
	
	// Update is called once per frame
	void Update () {

        
         if (Input.GetMouseButtonDown(0))
         {
             Ray ray = camera.ScreenPointToRay(Input.mousePosition);
             RaycastHit hit;
             if (Physics.Raycast(ray, out hit))
             {
                 hit.collider.gameObject.GetComponent<cellColor>().colorChange();
             }
         }
 
        
		// Iteroidaan muutoksia askel kerrallaan tai jatkuvasti.
		if (Input.GetKeyDown(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift))
		{
			updateCells();
			Debug.Log("Between turns");
		}
	}

    


	// Täytetään soluruudukot.
	void FillCells()
	{
		for (int i = 0; i < sizeofCellGrid; i++)
		for (int j = 0; j < sizeofCellGrid; j++) {
			cells [i,j] = (GameObject)Instantiate (cellPrefab, new Vector3 (i * 2, 0, j * 2), Quaternion.identity);
			tmpCells [i,j] = (GameObject)Instantiate (cellPrefab, new Vector3 (i * 2, 0, j * 2), Quaternion.identity);
			}
	}

	// Luodaan aloitussolut.
    	void setUpCells()
        {
            /*   Color color = cells [5,5].renderer.material.color;
                

               
                
                cells [5,5].renderer.material.color = color;
                cells [5,6].renderer.material.color = color;
                cells [6,4].renderer.material.color = color; 
                cells [6,5].renderer.material.color = color;

                cells [3,4].renderer.material.color = color;
                cells [4,4].renderer.material.color = color;
                cells [5,1].renderer.material.color = color;*/
        }

    // Päivitetään solut.
	public void updateCells()
	{
		// Jokaiselle solulle ... 
		for (int i = 0; i < sizeofCellGrid; i++)
			for (int j = 0; j < sizeofCellGrid; j++) 
			{
				// Siirretään aiemman iteraation oletustila muutoksia säilövään tilapäiseen soluruudukkoon.
				tmpCells[i,j].renderer.material.color = cells[i,j].renderer.material.color;

				// Lasketaan elävät ja kuolleet naapurit.
				int NumberOfDeadNeighbourCells = 0;
				int NumberOfAliveNeighbourCells = 0;

				for (int k = i-1; k <i+2; k++)
					for (int l = j-1; l < j+2; l++)
					{
				if (k >=0 && k < sizeofCellGrid && l >=0 && l < sizeofCellGrid && !(k==i && l==j))
						{
							if (cells[k,l].renderer.material.color.a == 0.1f)

								NumberOfDeadNeighbourCells++;
							if (cells[k,l].renderer.material.color.a == 1.0f)
								NumberOfAliveNeighbourCells++;
						}
					}
				
				// Päivitetään tarkasteltavan solun tilanne elävien ja kuolleiden naapurien mukaan ja säilötään tilapäiseen soluruudukkoon.
				if (cells[i,j].renderer.material.color.a < 1.0f)
					if (NumberOfAliveNeighbourCells == 3)
					{
						Color color = tmpCells[i,j].renderer.material.color;
						color.a = 1.0f;
						tmpCells[i,j].renderer.material.color = color;
						Debug.Log("Pawn is alive" + i + j);
					}
				if (cells[i,j].renderer.material.color.a == 1.0f)
					if (NumberOfAliveNeighbourCells < 2 || NumberOfAliveNeighbourCells > 3)
					{
						Color color = tmpCells[i,j].renderer.material.color;
						color.a = 0.1f;
						tmpCells[i,j].renderer.material.color = color;
						Debug.Log("Pawn is dead" + i + j);
					}
			}

		// Korvataan alkuperäisen solutaulukon solujen arvot tilapäisen solutaulukon arvoilla.
		for (int i = 0; i < sizeofCellGrid; i++)
			for (int j = 0; j < sizeofCellGrid; j++) 
				cells[i,j].renderer.material.color = tmpCells[i,j].renderer.material.color;
	}

}
